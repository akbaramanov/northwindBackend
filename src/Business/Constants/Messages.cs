﻿namespace Business.Constants
{
    public static class Messages
    {
        public static string ProductAdded => "Product Successfully Added";
        public static string ProductDeleted => "Product Successfully Deleted";
        public static string ProductUpdated => "Product Successfully Updated";
      
    }
}
