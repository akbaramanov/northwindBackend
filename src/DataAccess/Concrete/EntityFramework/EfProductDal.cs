﻿using Core.EntityFramework;
using DataAccess.Abstract;
using Entity.Concrete.EntityFramework.Contexts;
namespace Entity.Concrete.EntityFramework
{
    public class EfProductDal : EfEntityRepositoryBase<Product, NorthWindContext>,IProductDal
    {
    }
}
